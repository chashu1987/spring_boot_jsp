package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintBootJspApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintBootJspApplication.class, args);
	}

}
